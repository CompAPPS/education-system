﻿namespace OOP_Project
{
    partial class formCourse
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroTabControl1 = new MetroFramework.Controls.MetroTabControl();
            this.formCourses_tabCourseInfo = new MetroFramework.Controls.MetroTabPage();
            this.create_assignment_button = new MetroFramework.Controls.MetroButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.formCourse_courseInfo_labelName = new MetroFramework.Controls.MetroLabel();
            this.formCourse_courseInfo_labelDescription = new MetroFramework.Controls.MetroLabel();
            this.formCourse_courseInfo_labelCourseID = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.formCourses_tabAssignments = new MetroFramework.Controls.MetroTabPage();
//            this.Ass_listView = new System.Windows.Forms.ListView();
            this.formCourse_btnBack = new MetroFramework.Controls.MetroButton();
            this.metroTabControl1.SuspendLayout();
            this.formCourses_tabCourseInfo.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.formCourses_tabAssignments.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroTabControl1
            // 
            this.metroTabControl1.Controls.Add(this.formCourses_tabCourseInfo);
            this.metroTabControl1.Controls.Add(this.formCourses_tabAssignments);
            this.metroTabControl1.FontWeight = MetroFramework.MetroTabControlWeight.Bold;
            this.metroTabControl1.Location = new System.Drawing.Point(24, 64);
            this.metroTabControl1.Name = "metroTabControl1";
            this.metroTabControl1.SelectedIndex = 0;
            this.metroTabControl1.Size = new System.Drawing.Size(853, 513);
            this.metroTabControl1.Style = MetroFramework.MetroColorStyle.Black;
            this.metroTabControl1.TabIndex = 0;
            this.metroTabControl1.Theme = MetroFramework.MetroThemeStyle.Light;
            // 
            // formCourses_tabCourseInfo
            // 
            this.formCourses_tabCourseInfo.Controls.Add(this.create_assignment_button);
            this.formCourses_tabCourseInfo.Controls.Add(this.groupBox1);
            this.formCourses_tabCourseInfo.HorizontalScrollbarBarColor = true;
            this.formCourses_tabCourseInfo.Location = new System.Drawing.Point(4, 35);
            this.formCourses_tabCourseInfo.Name = "formCourses_tabCourseInfo";
            this.formCourses_tabCourseInfo.Size = new System.Drawing.Size(845, 474);
            this.formCourses_tabCourseInfo.TabIndex = 0;
            this.formCourses_tabCourseInfo.Text = "                        Course Info                        ";
            this.formCourses_tabCourseInfo.VerticalScrollbarBarColor = true;
            this.formCourses_tabCourseInfo.Click += new System.EventHandler(this.formCourses_tabCourseInfo_Click_1);
            // 
            // create_assignment_button
            // 
            this.create_assignment_button.Location = new System.Drawing.Point(697, 426);
            this.create_assignment_button.Name = "create_assignment_button";
            this.create_assignment_button.Size = new System.Drawing.Size(119, 33);
            this.create_assignment_button.TabIndex = 3;
            this.create_assignment_button.Text = "Create assignment";
            this.create_assignment_button.Visible = false;
            this.create_assignment_button.Click += new System.EventHandler(this.create_assignment_button_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox1.Controls.Add(this.formCourse_courseInfo_labelName);
            this.groupBox1.Controls.Add(this.formCourse_courseInfo_labelDescription);
            this.groupBox1.Controls.Add(this.formCourse_courseInfo_labelCourseID);
            this.groupBox1.Controls.Add(this.metroLabel3);
            this.groupBox1.Controls.Add(this.metroLabel2);
            this.groupBox1.Controls.Add(this.metroLabel1);
            this.groupBox1.Location = new System.Drawing.Point(4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(812, 406);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // formCourse_courseInfo_labelName
            // 
            this.formCourse_courseInfo_labelName.AutoSize = true;
            this.formCourse_courseInfo_labelName.Location = new System.Drawing.Point(118, 70);
            this.formCourse_courseInfo_labelName.Name = "formCourse_courseInfo_labelName";
            this.formCourse_courseInfo_labelName.Size = new System.Drawing.Size(13, 19);
            this.formCourse_courseInfo_labelName.TabIndex = 13;
            this.formCourse_courseInfo_labelName.Text = " ";
            // 
            // formCourse_courseInfo_labelDescription
            // 
            this.formCourse_courseInfo_labelDescription.AutoSize = true;
            this.formCourse_courseInfo_labelDescription.Location = new System.Drawing.Point(118, 121);
            this.formCourse_courseInfo_labelDescription.Name = "formCourse_courseInfo_labelDescription";
            this.formCourse_courseInfo_labelDescription.Size = new System.Drawing.Size(13, 19);
            this.formCourse_courseInfo_labelDescription.TabIndex = 12;
            this.formCourse_courseInfo_labelDescription.Text = " ";
            // 
            // formCourse_courseInfo_labelCourseID
            // 
            this.formCourse_courseInfo_labelCourseID.AutoSize = true;
            this.formCourse_courseInfo_labelCourseID.Location = new System.Drawing.Point(118, 15);
            this.formCourse_courseInfo_labelCourseID.Name = "formCourse_courseInfo_labelCourseID";
            this.formCourse_courseInfo_labelCourseID.Size = new System.Drawing.Size(13, 19);
            this.formCourse_courseInfo_labelCourseID.TabIndex = 11;
            this.formCourse_courseInfo_labelCourseID.Text = " ";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(6, 121);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(77, 19);
            this.metroLabel3.TabIndex = 10;
            this.metroLabel3.Text = "Description:";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(6, 70);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(48, 19);
            this.metroLabel2.TabIndex = 9;
            this.metroLabel2.Text = "Name:";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(6, 16);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(69, 19);
            this.metroLabel1.TabIndex = 8;
            this.metroLabel1.Text = "Course ID:";
            // 
            // formCourses_tabAssignments
            // 
            this.formCourses_tabAssignments.Controls.Add(this.Ass_listView);
            this.formCourses_tabAssignments.HorizontalScrollbarBarColor = true;
            this.formCourses_tabAssignments.Location = new System.Drawing.Point(4, 35);
            this.formCourses_tabAssignments.Name = "formCourses_tabAssignments";
            this.formCourses_tabAssignments.Size = new System.Drawing.Size(845, 474);
            this.formCourses_tabAssignments.TabIndex = 1;
            this.formCourses_tabAssignments.Text = "                        Assignments                        ";
            this.formCourses_tabAssignments.VerticalScrollbarBarColor = true;
            this.formCourses_tabAssignments.Click += new System.EventHandler(this.formCourses_tabCourseInfo_Click);
            // 
            // Ass_listView
            // 
            this.Ass_listView.Location = new System.Drawing.Point(23, 20);
            this.Ass_listView.Name = "Ass_listView";
            //this.Ass_listView.RoundedCornersMaskListItem = ((byte)(15));
            this.Ass_listView.Size = new System.Drawing.Size(786, 391);
            this.Ass_listView.TabIndex = 2;
            this.Ass_listView.Text = "vListBox1";
            //this.Ass_listView.VIBlendScrollBarsTheme = VIBlend.Utilities.VIBLEND_THEME.EXPRESSIONDARK;
            //this.Ass_listView.VIBlendTheme = VIBlend.Utilities.VIBLEND_THEME.EXPRESSIONDARK;
            //this.Ass_listView.ItemMouseDown += new System.EventHandler<VIBlend.WinForms.Controls.ListItemMouseEventArgs>(this.vListBox1_ItemMouseDown);
            this.Ass_listView.Click += new System.EventHandler(this.Ass_listView_Click);
            // 
            // formCourse_btnBack
            // 
            this.formCourse_btnBack.Location = new System.Drawing.Point(777, 33);
            this.formCourse_btnBack.Name = "formCourse_btnBack";
            this.formCourse_btnBack.Size = new System.Drawing.Size(100, 25);
            this.formCourse_btnBack.TabIndex = 1;
            this.formCourse_btnBack.Text = "Back";
            this.formCourse_btnBack.Click += new System.EventHandler(this.formCourse_btnBack_Click);
            // 
            // formCourse
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(900, 600);
            this.Controls.Add(this.formCourse_btnBack);
            this.Controls.Add(this.metroTabControl1);
            this.MaximizeBox = false;
            this.Name = "formCourse";
            this.Resizable = false;
            this.Style = MetroFramework.MetroColorStyle.Black;
            this.Text = "Course";
            this.Theme = MetroFramework.MetroThemeStyle.Light;
            this.Load += new System.EventHandler(this.Course_Load);
            this.metroTabControl1.ResumeLayout(false);
            this.formCourses_tabCourseInfo.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.formCourses_tabAssignments.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroTabControl metroTabControl1;
        private MetroFramework.Controls.MetroTabPage formCourses_tabCourseInfo;
        private MetroFramework.Controls.MetroTabPage formCourses_tabAssignments;
        private System.Windows.Forms.GroupBox groupBox1;
        private MetroFramework.Controls.MetroLabel formCourse_courseInfo_labelName;
        private MetroFramework.Controls.MetroLabel formCourse_courseInfo_labelDescription;
        private MetroFramework.Controls.MetroLabel formCourse_courseInfo_labelCourseID;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroButton create_assignment_button;
        private System.Windows.Forms.ListView Ass_listView;
        private MetroFramework.Controls.MetroButton formCourse_btnBack;

    }
}