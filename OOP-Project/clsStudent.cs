﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MetroFramework.Forms;
using System.Windows.Forms;

namespace OOP_Project
{
    class clsStudent : clsPerson
    {
        string name, email, password, gender;
    
        public clsStudent()
        {
        }
        public void setName(string n)
        {
            name = n;
        }
        public void setMail(string mail)
        {
            email = mail;
        }
        public void setUsername(string un)
        {
            username = un;
        }
        public void setPassword(string ps1)
        {
             password = ps1;
           
        }
        public void setGender(string g)
        {
            gender = g;
        }
        public  string getName()
        {
            return name;
        }
        public string getEMail()
        {
            return email;
        }
        public string getusername()
        {
            return username;
        }
        public string getPassword()
        {
            return password;
        }
        public string getGender()
        {
            return gender;
        }
    }
}
