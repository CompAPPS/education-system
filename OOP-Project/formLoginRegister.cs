﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using System.Data.SqlClient;
namespace OOP_Project
{
    public partial class formLoginRegister : MetroForm
    {
        SqlConnection con = new SqlConnection(@"Data Source=AHMED\SQLEXPRESS;Initial Catalog=oop_project;Integrated Security=True");
        SqlCommand cmd = new SqlCommand();
        SqlDataReader dr;
        clsInstructor instructor = new clsInstructor();
        clsStudent student = new clsStudent();
        public formLoginRegister()
        {
            InitializeComponent();
            cmd.Connection = con;
        }
        private void Form1_Load(object sender, EventArgs e)
        {

        }
        private void formLoginRegister_register_btnRegister_Click(object sender, EventArgs e)
        {
            if (formLoginRegister_register_textboxPassword1.Text == formLoginRegister_register_textboxPassword2.Text
                && formLoginRegister_register_textboxEMail.Text != ""
                && formLoginRegister_register_textboxName.Text != ""
                && formLoginRegister_register_textboxUsername.Text != ""
                && formLoginRegister_register_comboBoxGender.Text != ""
                && username_label.Visible == false)
            {
                if (formLoginRegister_register_radiobtnStudent.Checked)
                {
                    student.setName(formLoginRegister_register_textboxName.Text);
                    student.setMail(formLoginRegister_register_textboxEMail.Text);
                    student.setPassword(formLoginRegister_register_textboxPassword1.Text);
                    student.setUsername(formLoginRegister_register_textboxUsername.Text);
                    student.setGender(formLoginRegister_register_comboBoxGender.Text);
                }
                else if (formLoginRegister_register_radiobtnInstructor.Checked)
                {
                    instructor.setName(formLoginRegister_register_textboxName.Text);
                    instructor.setMail(formLoginRegister_register_textboxEMail.Text);
                    instructor.setPassword(formLoginRegister_register_textboxPassword1.Text);
                    instructor.setUsername(formLoginRegister_register_textboxUsername.Text);
                    instructor.setGender(formLoginRegister_register_comboBoxGender.Text);
                }
                con.Open();
                string acc_type;
                if (formLoginRegister_register_radiobtnStudent.Checked)
                    acc_type = "student";
                else
                    acc_type = "instructor";

                if (acc_type == "student")
                    cmd.CommandText = "insert into Person values('" + formLoginRegister_register_textboxName.Text + "','" + formLoginRegister_register_textboxUsername.Text +
                        "','" + formLoginRegister_register_textboxEMail.Text+ "','" + formLoginRegister_register_textboxPassword1.Text + "','" + formLoginRegister_register_comboBoxGender.Text + "','" + acc_type + "')";
                else
                    cmd.CommandText = "insert into Person values('" + formLoginRegister_register_textboxName.Text + "','" + formLoginRegister_register_textboxUsername.Text +
                                       "','" + formLoginRegister_register_textboxEMail.Text + "','" + formLoginRegister_register_textboxPassword1.Text + "','" + formLoginRegister_register_comboBoxGender.Text + "','" + acc_type + "')";
                cmd.ExecuteNonQuery();
                con.Close();
                MessageBox.Show("Done");
            }
            else{
                MetroMessageBox box = new MetroMessageBox();
                box.Text = "Please make sure you have filled all the feilds";
                box.Show();
            }
        }

        private void formLoginRegister_register_textboxPassword2_TextChanged(object sender, EventArgs e)
        {
            if (formLoginRegister_register_textboxPassword2.Text != formLoginRegister_register_textboxPassword1.Text)
            {
                formLoginRegister_register_labelPasswordsDontMatch.Show();
            }
            if (formLoginRegister_register_textboxPassword2.Text == formLoginRegister_register_textboxPassword1.Text)
            {
                formLoginRegister_register_labelPasswordsDontMatch.Hide();
            }
        }

        private void formLoginRegister_tabRegister_Click(object sender, EventArgs e)
        {

        }

        private void formLoginRegister_register_labelPasswordsDontMatch_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void formLoginRegister_tabLogin_Click(object sender, EventArgs e)
        {

        }

        private void formLoginRegister_login_btnLogin_Click(object sender, EventArgs e) 
        {
            if (TestCheckBox.Checked)
            {
                formHomepage x = new formHomepage();
                this.Hide();
                x.Show();
            }
            con.Open();
            string query = "SELECT * FROM Person WHERE Username ='" + formLoginRegister_login_textBoxUsername.Text + "'";
            SqlCommand cmd = new SqlCommand(query, con);
            dr = cmd.ExecuteReader();
            string ans = "";
            bool n = false;
            while (dr.Read())
            {
                ans = dr.GetString(3);
                if (ans == formLoginRegister_login_textboxPassword.Text)
                {
                    if(dr.GetString(5)=="student")
                    {
                        clsStudent userr = new clsStudent();
                        userr.setName(dr.GetString(0));
                        userr.setUsername(dr.GetString(1));
                        userr.setMail(dr.GetString(2));
                        userr.setGender(dr.GetString(4));
                        userr.set_type(dr.GetString(5));
                    }
                    else
                    {
                        clsInstructor userr = new clsInstructor();
                        userr.setName(dr.GetString(0));
                        userr.setUsername(dr.GetString(1));
                        userr.setMail(dr.GetString(2));
                        userr.setGender(dr.GetString(4));
                        userr.set_type(dr.GetString(5));
                    }

                    formHomepage x = new formHomepage();
                    this.Hide();
                    x.Show();
                    n = true;
                }
            }
            if (!n)
            {
                formloginRegister_invalid_label.Visible = true;
            }
            con.Close();
         
        }
        private void formLoginRegister_login_textboxPassword_Click(object sender, EventArgs e)
        {

        }

        private void formLoginRegister_login_textBoxUsername_Click(object sender, EventArgs e)
        {

        }

        private void metroLabel1_Click(object sender, EventArgs e)
        {

        }

        private void metroLabel2_Click(object sender, EventArgs e)
        {

        }

        private void username_label_Click(object sender, EventArgs e)
        {
        }

        private void formLoginRegister_register_textboxUsername_Click(object sender, EventArgs e)
        {

        }

        private void formLoginRegister_register_textboxUsername_TextChanged(object sender, EventArgs e)
        {
            con.Open();
            string query = "SELECT * FROM Person WHERE Username ='" + formLoginRegister_register_textboxUsername.Text + "'";
            SqlCommand cmd = new SqlCommand(query, con);
            dr = cmd.ExecuteReader();
            string str = "";
            while (dr.Read())
            {
                str = dr.GetString(1);
                if (formLoginRegister_register_textboxUsername.Text == str)
                    username_label.Show();
            }
            if (formLoginRegister_register_textboxUsername.Text != str)
                username_label.Hide();
            con.Close();
        }

        private void formLoginRegister_register_textboxPassword1_TextChanged(object sender, EventArgs e)
        {
        }

        private void formLoginRegister_login_textBoxUsername_TextChanged(object sender, EventArgs e)
        {
            formloginRegister_invalid_label.Visible = false;

        }
        private void formLoginRegister_login_textboxPassword_TextChanged(object sender, EventArgs e)
        {
            formloginRegister_invalid_label.Visible = false;

        }
    }
}