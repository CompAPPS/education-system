﻿namespace OOP_Project
{
    partial class formLoginRegister
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(formLoginRegister));
            this.formStartScreen_tabRegister = new MetroFramework.Controls.MetroTabControl();
            this.formLoginRegister_tabLogin = new MetroFramework.Controls.MetroTabPage();
            this.TestCheckBox = new System.Windows.Forms.CheckBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.formloginRegister_invalid_label = new MetroFramework.Controls.MetroLabel();
            this.formLoginRegister_login_btnLogin = new MetroFramework.Controls.MetroButton();
            this.formLoginRegister_login_textboxPassword = new MetroFramework.Controls.MetroTextBox();
            this.formLoginRegister_login_textBoxUsername = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.formLoginRegister_tabRegister = new MetroFramework.Controls.MetroTabPage();
            this.username_label = new MetroFramework.Controls.MetroLabel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.formLoginRegister_register_labelPasswordsDontMatch = new MetroFramework.Controls.MetroLabel();
            this.formLoginRegister_register_btnRegister = new MetroFramework.Controls.MetroButton();
            this.formLoginRegister_register_comboBoxGender = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.formLoginRegister_register_textboxPassword2 = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.formLoginRegister_register_textboxPassword1 = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.formLoginRegister_register_textboxUsername = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.formLoginRegister_register_textboxEMail = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.formLoginRegister_register_textboxName = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.formLoginRegister_register_radiobtnInstructor = new MetroFramework.Controls.MetroRadioButton();
            this.formLoginRegister_register_radiobtnStudent = new MetroFramework.Controls.MetroRadioButton();
            this.pageSetupDialog1 = new System.Windows.Forms.PageSetupDialog();
            this.pageSetupDialog2 = new System.Windows.Forms.PageSetupDialog();
            this.pageSetupDialog3 = new System.Windows.Forms.PageSetupDialog();
            this.pageSetupDialog4 = new System.Windows.Forms.PageSetupDialog();
            this.pageSetupDialog5 = new System.Windows.Forms.PageSetupDialog();
            this.formStartScreen_tabRegister.SuspendLayout();
            this.formLoginRegister_tabLogin.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.formLoginRegister_tabRegister.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // formStartScreen_tabRegister
            // 
            this.formStartScreen_tabRegister.AllowDrop = true;
            this.formStartScreen_tabRegister.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.formStartScreen_tabRegister.Controls.Add(this.formLoginRegister_tabLogin);
            this.formStartScreen_tabRegister.Controls.Add(this.formLoginRegister_tabRegister);
            this.formStartScreen_tabRegister.FontWeight = MetroFramework.MetroTabControlWeight.Bold;
            this.formStartScreen_tabRegister.Location = new System.Drawing.Point(23, 64);
            this.formStartScreen_tabRegister.Name = "formStartScreen_tabRegister";
            this.formStartScreen_tabRegister.SelectedIndex = 0;
            this.formStartScreen_tabRegister.Size = new System.Drawing.Size(854, 513);
            this.formStartScreen_tabRegister.Style = MetroFramework.MetroColorStyle.Black;
            this.formStartScreen_tabRegister.TabIndex = 0;
            this.formStartScreen_tabRegister.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.formStartScreen_tabRegister.Theme = MetroFramework.MetroThemeStyle.Light;
            // 
            // formLoginRegister_tabLogin
            // 
            this.formLoginRegister_tabLogin.Controls.Add(this.TestCheckBox);
            this.formLoginRegister_tabLogin.Controls.Add(this.pictureBox3);
            this.formLoginRegister_tabLogin.Controls.Add(this.formloginRegister_invalid_label);
            this.formLoginRegister_tabLogin.Controls.Add(this.formLoginRegister_login_btnLogin);
            this.formLoginRegister_tabLogin.Controls.Add(this.formLoginRegister_login_textboxPassword);
            this.formLoginRegister_tabLogin.Controls.Add(this.formLoginRegister_login_textBoxUsername);
            this.formLoginRegister_tabLogin.Controls.Add(this.metroLabel2);
            this.formLoginRegister_tabLogin.Controls.Add(this.metroLabel1);
            this.formLoginRegister_tabLogin.HorizontalScrollbarBarColor = true;
            this.formLoginRegister_tabLogin.Location = new System.Drawing.Point(4, 35);
            this.formLoginRegister_tabLogin.Name = "formLoginRegister_tabLogin";
            this.formLoginRegister_tabLogin.Size = new System.Drawing.Size(846, 474);
            this.formLoginRegister_tabLogin.TabIndex = 0;
            this.formLoginRegister_tabLogin.Text = "                        Login                        ";
            this.formLoginRegister_tabLogin.VerticalScrollbarBarColor = true;
            this.formLoginRegister_tabLogin.Click += new System.EventHandler(this.formLoginRegister_tabLogin_Click);
            // 
            // TestCheckBox
            // 
            this.TestCheckBox.AutoSize = true;
            this.TestCheckBox.Location = new System.Drawing.Point(21, 17);
            this.TestCheckBox.Name = "TestCheckBox";
            this.TestCheckBox.Size = new System.Drawing.Size(47, 17);
            this.TestCheckBox.TabIndex = 11;
            this.TestCheckBox.Text = "Test";
            this.TestCheckBox.UseVisualStyleBackColor = true;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.Window;
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(526, 104);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(267, 265);
            this.pictureBox3.TabIndex = 10;
            this.pictureBox3.TabStop = false;
            // 
            // formloginRegister_invalid_label
            // 
            this.formloginRegister_invalid_label.AutoSize = true;
            this.formloginRegister_invalid_label.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.formloginRegister_invalid_label.Location = new System.Drawing.Point(44, 276);
            this.formloginRegister_invalid_label.Name = "formloginRegister_invalid_label";
            this.formloginRegister_invalid_label.Size = new System.Drawing.Size(210, 19);
            this.formloginRegister_invalid_label.TabIndex = 9;
            this.formloginRegister_invalid_label.Text = "Invalid username or password";
            this.formloginRegister_invalid_label.Visible = false;
            // 
            // formLoginRegister_login_btnLogin
            // 
            this.formLoginRegister_login_btnLogin.Location = new System.Drawing.Point(280, 270);
            this.formLoginRegister_login_btnLogin.Name = "formLoginRegister_login_btnLogin";
            this.formLoginRegister_login_btnLogin.Size = new System.Drawing.Size(111, 34);
            this.formLoginRegister_login_btnLogin.TabIndex = 7;
            this.formLoginRegister_login_btnLogin.Text = "Login";
            this.formLoginRegister_login_btnLogin.Click += new System.EventHandler(this.formLoginRegister_login_btnLogin_Click);
            // 
            // formLoginRegister_login_textboxPassword
            // 
            this.formLoginRegister_login_textboxPassword.Location = new System.Drawing.Point(133, 208);
            this.formLoginRegister_login_textboxPassword.Name = "formLoginRegister_login_textboxPassword";
            this.formLoginRegister_login_textboxPassword.PasswordChar = '●';
            this.formLoginRegister_login_textboxPassword.Size = new System.Drawing.Size(259, 23);
            this.formLoginRegister_login_textboxPassword.TabIndex = 5;
            this.formLoginRegister_login_textboxPassword.UseSystemPasswordChar = true;
            this.formLoginRegister_login_textboxPassword.TextChanged += new System.EventHandler(this.formLoginRegister_login_textboxPassword_TextChanged);
            this.formLoginRegister_login_textboxPassword.Click += new System.EventHandler(this.formLoginRegister_login_textboxPassword_Click);
            // 
            // formLoginRegister_login_textBoxUsername
            // 
            this.formLoginRegister_login_textBoxUsername.Location = new System.Drawing.Point(133, 156);
            this.formLoginRegister_login_textBoxUsername.Name = "formLoginRegister_login_textBoxUsername";
            this.formLoginRegister_login_textBoxUsername.Size = new System.Drawing.Size(259, 23);
            this.formLoginRegister_login_textBoxUsername.TabIndex = 4;
            this.formLoginRegister_login_textBoxUsername.TextChanged += new System.EventHandler(this.formLoginRegister_login_textBoxUsername_TextChanged);
            this.formLoginRegister_login_textBoxUsername.Click += new System.EventHandler(this.formLoginRegister_login_textBoxUsername_Click);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(21, 213);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(63, 19);
            this.metroLabel2.TabIndex = 3;
            this.metroLabel2.Text = "Password";
            this.metroLabel2.Click += new System.EventHandler(this.metroLabel2_Click);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(21, 160);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(68, 19);
            this.metroLabel1.TabIndex = 2;
            this.metroLabel1.Text = "Username";
            this.metroLabel1.Click += new System.EventHandler(this.metroLabel1_Click);
            // 
            // formLoginRegister_tabRegister
            // 
            this.formLoginRegister_tabRegister.Controls.Add(this.username_label);
            this.formLoginRegister_tabRegister.Controls.Add(this.pictureBox1);
            this.formLoginRegister_tabRegister.Controls.Add(this.formLoginRegister_register_labelPasswordsDontMatch);
            this.formLoginRegister_tabRegister.Controls.Add(this.formLoginRegister_register_btnRegister);
            this.formLoginRegister_tabRegister.Controls.Add(this.formLoginRegister_register_comboBoxGender);
            this.formLoginRegister_tabRegister.Controls.Add(this.metroLabel8);
            this.formLoginRegister_tabRegister.Controls.Add(this.formLoginRegister_register_textboxPassword2);
            this.formLoginRegister_tabRegister.Controls.Add(this.metroLabel7);
            this.formLoginRegister_tabRegister.Controls.Add(this.formLoginRegister_register_textboxPassword1);
            this.formLoginRegister_tabRegister.Controls.Add(this.metroLabel6);
            this.formLoginRegister_tabRegister.Controls.Add(this.formLoginRegister_register_textboxUsername);
            this.formLoginRegister_tabRegister.Controls.Add(this.metroLabel5);
            this.formLoginRegister_tabRegister.Controls.Add(this.formLoginRegister_register_textboxEMail);
            this.formLoginRegister_tabRegister.Controls.Add(this.metroLabel4);
            this.formLoginRegister_tabRegister.Controls.Add(this.formLoginRegister_register_textboxName);
            this.formLoginRegister_tabRegister.Controls.Add(this.metroLabel3);
            this.formLoginRegister_tabRegister.Controls.Add(this.formLoginRegister_register_radiobtnInstructor);
            this.formLoginRegister_tabRegister.Controls.Add(this.formLoginRegister_register_radiobtnStudent);
            this.formLoginRegister_tabRegister.HorizontalScrollbarBarColor = true;
            this.formLoginRegister_tabRegister.Location = new System.Drawing.Point(4, 35);
            this.formLoginRegister_tabRegister.Name = "formLoginRegister_tabRegister";
            this.formLoginRegister_tabRegister.Size = new System.Drawing.Size(846, 474);
            this.formLoginRegister_tabRegister.TabIndex = 1;
            this.formLoginRegister_tabRegister.Text = "                        Register                        ";
            this.formLoginRegister_tabRegister.VerticalScrollbarBarColor = true;
            this.formLoginRegister_tabRegister.Click += new System.EventHandler(this.formLoginRegister_tabRegister_Click);
            // 
            // username_label
            // 
            this.username_label.AutoSize = true;
            this.username_label.FontSize = MetroFramework.MetroLabelSize.Small;
            this.username_label.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.username_label.Location = new System.Drawing.Point(594, 134);
            this.username_label.Name = "username_label";
            this.username_label.Size = new System.Drawing.Size(165, 15);
            this.username_label.TabIndex = 19;
            this.username_label.Text = "This username arleady exists";
            this.username_label.Visible = false;
            this.username_label.Click += new System.EventHandler(this.username_label_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Window;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(-4, 43);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(264, 310);
            this.pictureBox1.TabIndex = 18;
            this.pictureBox1.TabStop = false;
            // 
            // formLoginRegister_register_labelPasswordsDontMatch
            // 
            this.formLoginRegister_register_labelPasswordsDontMatch.AutoSize = true;
            this.formLoginRegister_register_labelPasswordsDontMatch.FontSize = MetroFramework.MetroLabelSize.Small;
            this.formLoginRegister_register_labelPasswordsDontMatch.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.formLoginRegister_register_labelPasswordsDontMatch.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.formLoginRegister_register_labelPasswordsDontMatch.Location = new System.Drawing.Point(594, 225);
            this.formLoginRegister_register_labelPasswordsDontMatch.Name = "formLoginRegister_register_labelPasswordsDontMatch";
            this.formLoginRegister_register_labelPasswordsDontMatch.Size = new System.Drawing.Size(134, 15);
            this.formLoginRegister_register_labelPasswordsDontMatch.Style = MetroFramework.MetroColorStyle.Red;
            this.formLoginRegister_register_labelPasswordsDontMatch.TabIndex = 17;
            this.formLoginRegister_register_labelPasswordsDontMatch.Text = "Passwords don\'t match";
            this.formLoginRegister_register_labelPasswordsDontMatch.Theme = MetroFramework.MetroThemeStyle.Light;
            this.formLoginRegister_register_labelPasswordsDontMatch.Visible = false;
            this.formLoginRegister_register_labelPasswordsDontMatch.Click += new System.EventHandler(this.formLoginRegister_register_labelPasswordsDontMatch_Click);
            // 
            // formLoginRegister_register_btnRegister
            // 
            this.formLoginRegister_register_btnRegister.Location = new System.Drawing.Point(709, 319);
            this.formLoginRegister_register_btnRegister.Name = "formLoginRegister_register_btnRegister";
            this.formLoginRegister_register_btnRegister.Size = new System.Drawing.Size(111, 34);
            this.formLoginRegister_register_btnRegister.TabIndex = 16;
            this.formLoginRegister_register_btnRegister.Text = "Register";
            this.formLoginRegister_register_btnRegister.Click += new System.EventHandler(this.formLoginRegister_register_btnRegister_Click);
            // 
            // formLoginRegister_register_comboBoxGender
            // 
            this.formLoginRegister_register_comboBoxGender.FormattingEnabled = true;
            this.formLoginRegister_register_comboBoxGender.ItemHeight = 23;
            this.formLoginRegister_register_comboBoxGender.Items.AddRange(new object[] {
            "Male",
            "Female"});
            this.formLoginRegister_register_comboBoxGender.Location = new System.Drawing.Point(594, 268);
            this.formLoginRegister_register_comboBoxGender.Name = "formLoginRegister_register_comboBoxGender";
            this.formLoginRegister_register_comboBoxGender.Size = new System.Drawing.Size(121, 29);
            this.formLoginRegister_register_comboBoxGender.TabIndex = 15;
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(438, 268);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(52, 19);
            this.metroLabel8.TabIndex = 14;
            this.metroLabel8.Text = "Gender";
            // 
            // formLoginRegister_register_textboxPassword2
            // 
            this.formLoginRegister_register_textboxPassword2.Location = new System.Drawing.Point(594, 200);
            this.formLoginRegister_register_textboxPassword2.Name = "formLoginRegister_register_textboxPassword2";
            this.formLoginRegister_register_textboxPassword2.PasswordChar = '●';
            this.formLoginRegister_register_textboxPassword2.Size = new System.Drawing.Size(226, 23);
            this.formLoginRegister_register_textboxPassword2.TabIndex = 13;
            this.formLoginRegister_register_textboxPassword2.UseSystemPasswordChar = true;
            this.formLoginRegister_register_textboxPassword2.TextChanged += new System.EventHandler(this.formLoginRegister_register_textboxPassword2_TextChanged);
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(436, 200);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(116, 19);
            this.metroLabel7.TabIndex = 12;
            this.metroLabel7.Text = "Confirm password";
            // 
            // formLoginRegister_register_textboxPassword1
            // 
            this.formLoginRegister_register_textboxPassword1.Location = new System.Drawing.Point(594, 154);
            this.formLoginRegister_register_textboxPassword1.Name = "formLoginRegister_register_textboxPassword1";
            this.formLoginRegister_register_textboxPassword1.PasswordChar = '●';
            this.formLoginRegister_register_textboxPassword1.Size = new System.Drawing.Size(226, 23);
            this.formLoginRegister_register_textboxPassword1.TabIndex = 11;
            this.formLoginRegister_register_textboxPassword1.UseSystemPasswordChar = true;
            this.formLoginRegister_register_textboxPassword1.TextChanged += new System.EventHandler(this.formLoginRegister_register_textboxPassword1_TextChanged);
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(438, 154);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(63, 19);
            this.metroLabel6.TabIndex = 10;
            this.metroLabel6.Text = "Password";
            // 
            // formLoginRegister_register_textboxUsername
            // 
            this.formLoginRegister_register_textboxUsername.Location = new System.Drawing.Point(594, 109);
            this.formLoginRegister_register_textboxUsername.Name = "formLoginRegister_register_textboxUsername";
            this.formLoginRegister_register_textboxUsername.Size = new System.Drawing.Size(226, 23);
            this.formLoginRegister_register_textboxUsername.TabIndex = 9;
            this.formLoginRegister_register_textboxUsername.TextChanged += new System.EventHandler(this.formLoginRegister_register_textboxUsername_TextChanged);
            this.formLoginRegister_register_textboxUsername.Click += new System.EventHandler(this.formLoginRegister_register_textboxUsername_Click);
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(438, 109);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(68, 19);
            this.metroLabel5.TabIndex = 8;
            this.metroLabel5.Text = "Username";
            // 
            // formLoginRegister_register_textboxEMail
            // 
            this.formLoginRegister_register_textboxEMail.Location = new System.Drawing.Point(594, 68);
            this.formLoginRegister_register_textboxEMail.Name = "formLoginRegister_register_textboxEMail";
            this.formLoginRegister_register_textboxEMail.Size = new System.Drawing.Size(226, 23);
            this.formLoginRegister_register_textboxEMail.TabIndex = 7;
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(438, 68);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(47, 19);
            this.metroLabel4.TabIndex = 6;
            this.metroLabel4.Text = "E-Mail";
            // 
            // formLoginRegister_register_textboxName
            // 
            this.formLoginRegister_register_textboxName.Location = new System.Drawing.Point(594, 26);
            this.formLoginRegister_register_textboxName.Name = "formLoginRegister_register_textboxName";
            this.formLoginRegister_register_textboxName.Size = new System.Drawing.Size(226, 23);
            this.formLoginRegister_register_textboxName.TabIndex = 5;
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(438, 30);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(45, 19);
            this.metroLabel3.TabIndex = 4;
            this.metroLabel3.Text = "Name";
            // 
            // formLoginRegister_register_radiobtnInstructor
            // 
            this.formLoginRegister_register_radiobtnInstructor.AutoSize = true;
            this.formLoginRegister_register_radiobtnInstructor.Location = new System.Drawing.Point(279, 68);
            this.formLoginRegister_register_radiobtnInstructor.Name = "formLoginRegister_register_radiobtnInstructor";
            this.formLoginRegister_register_radiobtnInstructor.Size = new System.Drawing.Size(74, 15);
            this.formLoginRegister_register_radiobtnInstructor.TabIndex = 3;
            this.formLoginRegister_register_radiobtnInstructor.Text = "Instructor";
            this.formLoginRegister_register_radiobtnInstructor.UseVisualStyleBackColor = true;
            // 
            // formLoginRegister_register_radiobtnStudent
            // 
            this.formLoginRegister_register_radiobtnStudent.AutoSize = true;
            this.formLoginRegister_register_radiobtnStudent.Checked = true;
            this.formLoginRegister_register_radiobtnStudent.Location = new System.Drawing.Point(279, 43);
            this.formLoginRegister_register_radiobtnStudent.Name = "formLoginRegister_register_radiobtnStudent";
            this.formLoginRegister_register_radiobtnStudent.Size = new System.Drawing.Size(64, 15);
            this.formLoginRegister_register_radiobtnStudent.TabIndex = 2;
            this.formLoginRegister_register_radiobtnStudent.TabStop = true;
            this.formLoginRegister_register_radiobtnStudent.Text = "Student";
            this.formLoginRegister_register_radiobtnStudent.UseVisualStyleBackColor = true;
            // 
            // formLoginRegister
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(900, 600);
            this.Controls.Add(this.formStartScreen_tabRegister);
            this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.MaximizeBox = false;
            this.Name = "formLoginRegister";
            this.Resizable = false;
            this.Style = MetroFramework.MetroColorStyle.Black;
            this.Text = "Login/Register";
            this.Theme = MetroFramework.MetroThemeStyle.Light;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.formStartScreen_tabRegister.ResumeLayout(false);
            this.formLoginRegister_tabLogin.ResumeLayout(false);
            this.formLoginRegister_tabLogin.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.formLoginRegister_tabRegister.ResumeLayout(false);
            this.formLoginRegister_tabRegister.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroTabControl formStartScreen_tabRegister;
        private MetroFramework.Controls.MetroTabPage formLoginRegister_tabLogin;
        private MetroFramework.Controls.MetroButton formLoginRegister_login_btnLogin;
        private MetroFramework.Controls.MetroTextBox formLoginRegister_login_textboxPassword;
        private MetroFramework.Controls.MetroTextBox formLoginRegister_login_textBoxUsername;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTabPage formLoginRegister_tabRegister;
        private MetroFramework.Controls.MetroRadioButton formLoginRegister_register_radiobtnInstructor;
        private MetroFramework.Controls.MetroRadioButton formLoginRegister_register_radiobtnStudent;
        private MetroFramework.Controls.MetroTextBox formLoginRegister_register_textboxName;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroTextBox formLoginRegister_register_textboxPassword2;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroTextBox formLoginRegister_register_textboxPassword1;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroTextBox formLoginRegister_register_textboxUsername;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroButton formLoginRegister_register_btnRegister;
        private MetroFramework.Controls.MetroComboBox formLoginRegister_register_comboBoxGender;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroLabel formLoginRegister_register_labelPasswordsDontMatch;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PageSetupDialog pageSetupDialog1;
        private System.Windows.Forms.PageSetupDialog pageSetupDialog2;
        private System.Windows.Forms.PageSetupDialog pageSetupDialog3;
        private System.Windows.Forms.PageSetupDialog pageSetupDialog4;
        private System.Windows.Forms.PageSetupDialog pageSetupDialog5;
        private MetroFramework.Controls.MetroTextBox formLoginRegister_register_textboxEMail;
        private MetroFramework.Controls.MetroLabel username_label;
        private MetroFramework.Controls.MetroLabel formloginRegister_invalid_label;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.CheckBox TestCheckBox;
    }
}

