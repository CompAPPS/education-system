﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using System.Data.SqlClient;
namespace OOP_Project
{
    public partial class formCourse : MetroForm
    {
        SqlConnection con = new SqlConnection(@"Data Source=AHMED\SQLEXPRESS;Initial Catalog=oop_project;Integrated Security=True;MultipleActiveResultSets=True;");
        SqlCommand cmd = new SqlCommand();
        SqlDataReader dr;
        List<long> AssignmentID=new List<long>();
        public formCourse()
        {
            
            InitializeComponent();
            cmd.Connection = con;
            SqlCommand cmdd = new SqlCommand();
            SqlDataReader DR;
            string Querk = "SELECT * FROM Assignment WHERE courseID ='" + clsCourse.ID + "'";
            List<string> AssList = new List<string>();
            List<clasAssignment> AssiObjList = new List<clasAssignment>();
            con.Open();
            cmdd = new SqlCommand(Querk, con);
            DR = cmdd.ExecuteReader();
            while(DR.Read())
            {
                AssList.Add(DR.GetInt64(0).ToString() + " " + DR.GetInt64(1).ToString() + " " + DR.GetInt64(2).ToString());
                clasAssignment a = new clasAssignment();
                a.courseID = DR.GetInt64(0);
                a.setNumQuestions(DR.GetInt64(1));
                AssignmentID.Add ( DR.GetInt64(2));
            }
            con.Close();
            for(int i=0;i<AssList.Count;i++)
            {
                ListViewItem item = new ListViewItem();
                item.Text = AssList[i];
                Ass_listView.Items.Add(item.Text);
            }
           
        }
        private void Course_Load(object sender, EventArgs e)
        {
            con.Open();
            string query = "SELECT * FROM Course WHERE courseID ='" +clsCourse.ID.ToString() + "'";
            SqlCommand cmd = new SqlCommand(query, con);
            dr = cmd.ExecuteReader();
            bool n=false;
            while (dr.Read())
            {
                formCourse_courseInfo_labelCourseID.Text = clsCourse.ID.ToString();
                formCourse_courseInfo_labelName.Text = dr.GetString(0);
                formCourse_courseInfo_labelDescription.Text = dr.GetString(1);
                n = true;
            }
            if (!n)
            {
                MetroMessageBox box = new MetroMessageBox();
                box.Text = "Invalid ID";
                box.Show();
            }
            con.Close();
            if (clsPerson.acc_type == "instructor")
            {
                create_assignment_button.Visible = true;
                
            }
        }

        private void formCourses_tabCourseInfo_Click(object sender, EventArgs e)
        {

        }

        private void formCourses_tabCourseInfo_Click_1(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void create_assignment_button_Click(object sender, EventArgs e)
        {
            formCreateAssignment x = new formCreateAssignment();
            this.Close();
            x.Show();
        }

        private void metroLabel4_Click(object sender, EventArgs e)
        {

        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void vListBox1_ItemMouseDown(object sender, EventArgs e)
        {
            int z = Ass_listView.SelectedIndices[0];
            this.Close();
            FormAssignment x = new FormAssignment(AssignmentID[z]);
            x.Show();
        }

        private void Ass_listView_Click(object sender, EventArgs e)
        {

        }

        private void formCourse_btnBack_Click(object sender, EventArgs e)
        {
            this.Close();
            formHomepage x = new formHomepage();
            x.Show();
        }
    }
}
