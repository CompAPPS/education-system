﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using System.Data.SqlClient;

namespace OOP_Project
{
    public partial class FormAssignment : MetroForm
    {
        List<clsQuestion> Questions = new List<clsQuestion>();
        List<string> assiNum = new List<string>();
        int offset = new int();
        int Score = 0;
        

        public FormAssignment(long AssignmentID)
        {
            InitializeComponent();
            SqlConnection con = new SqlConnection(@"Data Source=AHMED\SQLEXPRESS;Initial Catalog=oop_project;Integrated Security=True;MultipleActiveResultSets=True;");
            SqlCommand cmd = new SqlCommand();
            SqlDataReader dr;
            clsQuestion Q = new clsQuestion();
            string Query = "SELECT * FROM Question WHERE assignmentID ='" + AssignmentID.ToString() + "'";
            cmd.Connection = con;
            con.Open();
            cmd = new SqlCommand(Query, con);
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                Q = new clsQuestion();
                Q.setQuestion(dr.GetString(0));
                Q.setAnswers(dr.GetString(1), dr.GetString(2), dr.GetString(3), dr.GetString(4));
                //char[] x = dr.GetString(5).ToCharArray(); 
                Q.setCorrect(dr.GetString(5));
                Questions.Add(Q);
            }

            for (int i = 0; i < Questions.Count; i++)
            {
                assiNum.Add((i + 1).ToString());
            }
            ListViewItem item = new ListViewItem();
            for (int i = 0; i < Questions.Count; i++)
            {
                item.Text = assiNum[i];
                Ques_ListView.Items.Add(item.Text);
            }
            con.Close();
        }

        private void ViewAssignment_Load(object sender, EventArgs e)
        {


        }

        private void Ques_ListView_ItemMouseDown(object sender, EventArgs e)
        {
            Answer1_Rbtn.Visible = Answer2_Rbtn.Visible = Answer3_Rbtn.Visible = Answer4_Rbtn.Visible = Ques_Label.Visible = true;
            string str = Ques_ListView.SelectedItems[0].Text;
            for (int i = 0; i < Questions.Count; i++)
            {
                if (str == assiNum[i])
                {
                    offset = i;
                    break;
                }
            }
            Ques_Label.Text = Questions[offset].GetQuestion();
            Ques_Label.Show();
            string[] s = Questions[offset].GetAnswers();
            Answer1_Rbtn.Text = s[0];
            Answer2_Rbtn.Text = s[1];
            Answer3_Rbtn.Text = s[2];
            Answer4_Rbtn.Text = s[3];
            
        }

        private void Submit_Btn_Click(object sender, EventArgs e)
        {
            if ((Answer1_Rbtn.Checked && Questions[offset].correctAnswer == Answer1_Rbtn.Text) ||
                (Answer2_Rbtn.Checked && Questions[offset].correctAnswer == Answer2_Rbtn.Text)
                || (Answer3_Rbtn.Checked && Questions[offset].correctAnswer == Answer3_Rbtn.Text) ||
                (Answer4_Rbtn.Checked && Questions[offset].correctAnswer == Answer4_Rbtn.Text))
            {
                Score ++;
                Score_Lbl.Text = "Score: " + Score.ToString();
            }
            Ques_ListView.Items.RemoveAt(Ques_ListView.SelectedIndices[0]);
            Ques_Label.Text = "";
            Ques_Label.Show();
            Answer1_Rbtn.Text = "";
            Answer2_Rbtn.Text = "";
            Answer3_Rbtn.Text = "";
            Answer4_Rbtn.Text = "";
        }

        private void formAssignment_btnBack_Click(object sender, EventArgs e)
        {
            this.Close();
            formCourse x = new formCourse();
            x.Show();
        }
    }
}
