﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Project
{
    class clsCourse
    {
        public static string ID;
        string name;
        string description;

        public clsCourse()
        {

        }
        public string getName()
        {
            return name;
        }
        public string getDescription()
        {
            return description;
        }
        public void setName(string n)
        {
            name = n;
        }
        public void setDescription(string d)
        {
            description = d;
        }

    }
}

