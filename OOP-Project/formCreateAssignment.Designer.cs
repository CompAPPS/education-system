﻿namespace OOP_Project
{
    partial class formCreateAssignment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroTabControl1 = new MetroFramework.Controls.MetroTabControl();
            this.formCreateAssignment_create = new MetroFramework.Controls.MetroTabPage();
            this.formCreateAssignment_textboxAnswerD = new MetroFramework.Controls.MetroTextBox();
            this.formCreateAssignment_textboxAnswerC = new MetroFramework.Controls.MetroTextBox();
            this.formCreateAssignment_textboxAnswerB = new MetroFramework.Controls.MetroTextBox();
            this.formCreateAssignment_textboxAnswerA = new MetroFramework.Controls.MetroTextBox();
            this.formCreateAssignment_textboxQuestion = new MetroFramework.Controls.MetroTextBox();
            this.createAssignment_next_button = new MetroFramework.Controls.MetroButton();
            this.formCreateAssignment_textboxCorrectAnswer = new MetroFramework.Controls.MetroTextBox();
            this.question_label7_createAssinmentFom = new MetroFramework.Controls.MetroLabel();
            this.question_label6_createAssinmentFom = new MetroFramework.Controls.MetroLabel();
            this.question_label5_createAssinmentFom = new MetroFramework.Controls.MetroLabel();
            this.question_label4_createAssinmentFom = new MetroFramework.Controls.MetroLabel();
            this.question_label3_createAssinmentFom = new MetroFramework.Controls.MetroLabel();
            this.question_label2_createAssinmentFom = new MetroFramework.Controls.MetroLabel();
            this.question_label1_createAssinmentFom = new MetroFramework.Controls.MetroLabel();
            this.formCreateAssignment_btnCreateAssignment = new MetroFramework.Controls.MetroButton();
            this.formCreateAssignment_textboxNumQuestions = new MetroFramework.Controls.MetroTextBox();
            this.numberofquestions_label = new MetroFramework.Controls.MetroLabel();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.metroTabControl1.SuspendLayout();
            this.formCreateAssignment_create.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroTabControl1
            // 
            this.metroTabControl1.Controls.Add(this.formCreateAssignment_create);
            this.metroTabControl1.FontWeight = MetroFramework.MetroTabControlWeight.Bold;
            this.metroTabControl1.Location = new System.Drawing.Point(24, 64);
            this.metroTabControl1.Name = "metroTabControl1";
            this.metroTabControl1.SelectedIndex = 0;
            this.metroTabControl1.Size = new System.Drawing.Size(853, 483);
            this.metroTabControl1.Style = MetroFramework.MetroColorStyle.Black;
            this.metroTabControl1.TabIndex = 0;
            // 
            // formCreateAssignment_create
            // 
            this.formCreateAssignment_create.Controls.Add(this.listBox1);
            this.formCreateAssignment_create.Controls.Add(this.formCreateAssignment_textboxAnswerD);
            this.formCreateAssignment_create.Controls.Add(this.formCreateAssignment_textboxAnswerC);
            this.formCreateAssignment_create.Controls.Add(this.formCreateAssignment_textboxAnswerB);
            this.formCreateAssignment_create.Controls.Add(this.formCreateAssignment_textboxAnswerA);
            this.formCreateAssignment_create.Controls.Add(this.formCreateAssignment_textboxQuestion);
            this.formCreateAssignment_create.Controls.Add(this.createAssignment_next_button);
            this.formCreateAssignment_create.Controls.Add(this.formCreateAssignment_textboxCorrectAnswer);
            this.formCreateAssignment_create.Controls.Add(this.question_label7_createAssinmentFom);
            this.formCreateAssignment_create.Controls.Add(this.question_label6_createAssinmentFom);
            this.formCreateAssignment_create.Controls.Add(this.question_label5_createAssinmentFom);
            this.formCreateAssignment_create.Controls.Add(this.question_label4_createAssinmentFom);
            this.formCreateAssignment_create.Controls.Add(this.question_label3_createAssinmentFom);
            this.formCreateAssignment_create.Controls.Add(this.question_label2_createAssinmentFom);
            this.formCreateAssignment_create.Controls.Add(this.question_label1_createAssinmentFom);
            this.formCreateAssignment_create.Controls.Add(this.formCreateAssignment_btnCreateAssignment);
            this.formCreateAssignment_create.Controls.Add(this.formCreateAssignment_textboxNumQuestions);
            this.formCreateAssignment_create.Controls.Add(this.numberofquestions_label);
            this.formCreateAssignment_create.HorizontalScrollbarBarColor = true;
            this.formCreateAssignment_create.Location = new System.Drawing.Point(4, 35);
            this.formCreateAssignment_create.Name = "formCreateAssignment_create";
            this.formCreateAssignment_create.Size = new System.Drawing.Size(845, 444);
            this.formCreateAssignment_create.TabIndex = 0;
            this.formCreateAssignment_create.Text = "                        Create                        ";
            this.formCreateAssignment_create.VerticalScrollbarBarColor = true;
            // 
            // formCreateAssignment_textboxAnswerD
            // 
            this.formCreateAssignment_textboxAnswerD.Location = new System.Drawing.Point(71, 307);
            this.formCreateAssignment_textboxAnswerD.Multiline = true;
            this.formCreateAssignment_textboxAnswerD.Name = "formCreateAssignment_textboxAnswerD";
            this.formCreateAssignment_textboxAnswerD.Size = new System.Drawing.Size(736, 55);
            this.formCreateAssignment_textboxAnswerD.TabIndex = 18;
            this.formCreateAssignment_textboxAnswerD.Visible = false;
            // 
            // formCreateAssignment_textboxAnswerC
            // 
            this.formCreateAssignment_textboxAnswerC.Location = new System.Drawing.Point(71, 246);
            this.formCreateAssignment_textboxAnswerC.Multiline = true;
            this.formCreateAssignment_textboxAnswerC.Name = "formCreateAssignment_textboxAnswerC";
            this.formCreateAssignment_textboxAnswerC.Size = new System.Drawing.Size(736, 55);
            this.formCreateAssignment_textboxAnswerC.TabIndex = 17;
            this.formCreateAssignment_textboxAnswerC.Visible = false;
            // 
            // formCreateAssignment_textboxAnswerB
            // 
            this.formCreateAssignment_textboxAnswerB.Location = new System.Drawing.Point(71, 185);
            this.formCreateAssignment_textboxAnswerB.Multiline = true;
            this.formCreateAssignment_textboxAnswerB.Name = "formCreateAssignment_textboxAnswerB";
            this.formCreateAssignment_textboxAnswerB.Size = new System.Drawing.Size(736, 55);
            this.formCreateAssignment_textboxAnswerB.TabIndex = 16;
            this.formCreateAssignment_textboxAnswerB.Visible = false;
            // 
            // formCreateAssignment_textboxAnswerA
            // 
            this.formCreateAssignment_textboxAnswerA.Location = new System.Drawing.Point(71, 124);
            this.formCreateAssignment_textboxAnswerA.Multiline = true;
            this.formCreateAssignment_textboxAnswerA.Name = "formCreateAssignment_textboxAnswerA";
            this.formCreateAssignment_textboxAnswerA.Size = new System.Drawing.Size(736, 55);
            this.formCreateAssignment_textboxAnswerA.TabIndex = 15;
            this.formCreateAssignment_textboxAnswerA.Visible = false;
            // 
            // formCreateAssignment_textboxQuestion
            // 
            this.formCreateAssignment_textboxQuestion.Location = new System.Drawing.Point(71, 63);
            this.formCreateAssignment_textboxQuestion.Multiline = true;
            this.formCreateAssignment_textboxQuestion.Name = "formCreateAssignment_textboxQuestion";
            this.formCreateAssignment_textboxQuestion.Size = new System.Drawing.Size(736, 55);
            this.formCreateAssignment_textboxQuestion.TabIndex = 14;
            this.formCreateAssignment_textboxQuestion.Visible = false;
            this.formCreateAssignment_textboxQuestion.Click += new System.EventHandler(this.formCreateAssignment_textboxQuestion_Click);
            // 
            // createAssignment_next_button
            // 
            this.createAssignment_next_button.Location = new System.Drawing.Point(731, 408);
            this.createAssignment_next_button.Name = "createAssignment_next_button";
            this.createAssignment_next_button.Size = new System.Drawing.Size(111, 33);
            this.createAssignment_next_button.TabIndex = 13;
            this.createAssignment_next_button.Text = "Next";
            this.createAssignment_next_button.Visible = false;
            this.createAssignment_next_button.Click += new System.EventHandler(this.createAssignment_next_button_Click);
            // 
            // formCreateAssignment_textboxCorrectAnswer
            // 
            this.formCreateAssignment_textboxCorrectAnswer.Location = new System.Drawing.Point(108, 378);
            this.formCreateAssignment_textboxCorrectAnswer.Name = "formCreateAssignment_textboxCorrectAnswer";
            this.formCreateAssignment_textboxCorrectAnswer.Size = new System.Drawing.Size(103, 23);
            this.formCreateAssignment_textboxCorrectAnswer.TabIndex = 12;
            this.formCreateAssignment_textboxCorrectAnswer.Visible = false;
            // 
            // question_label7_createAssinmentFom
            // 
            this.question_label7_createAssinmentFom.AutoSize = true;
            this.question_label7_createAssinmentFom.FontSize = MetroFramework.MetroLabelSize.Small;
            this.question_label7_createAssinmentFom.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.question_label7_createAssinmentFom.Location = new System.Drawing.Point(229, 382);
            this.question_label7_createAssinmentFom.Name = "question_label7_createAssinmentFom";
            this.question_label7_createAssinmentFom.Size = new System.Drawing.Size(493, 15);
            this.question_label7_createAssinmentFom.TabIndex = 11;
            this.question_label7_createAssinmentFom.Text = "Please write only one letter representing the correct answer in upper case (A, B," +
    " C or D)";
            this.question_label7_createAssinmentFom.Visible = false;
            // 
            // question_label6_createAssinmentFom
            // 
            this.question_label6_createAssinmentFom.AutoSize = true;
            this.question_label6_createAssinmentFom.Location = new System.Drawing.Point(3, 378);
            this.question_label6_createAssinmentFom.Name = "question_label6_createAssinmentFom";
            this.question_label6_createAssinmentFom.Size = new System.Drawing.Size(99, 19);
            this.question_label6_createAssinmentFom.TabIndex = 10;
            this.question_label6_createAssinmentFom.Text = "Correct Answer";
            this.question_label6_createAssinmentFom.Visible = false;
            // 
            // question_label5_createAssinmentFom
            // 
            this.question_label5_createAssinmentFom.AutoSize = true;
            this.question_label5_createAssinmentFom.Location = new System.Drawing.Point(4, 307);
            this.question_label5_createAssinmentFom.Name = "question_label5_createAssinmentFom";
            this.question_label5_createAssinmentFom.Size = new System.Drawing.Size(18, 19);
            this.question_label5_createAssinmentFom.TabIndex = 9;
            this.question_label5_createAssinmentFom.Text = "D";
            this.question_label5_createAssinmentFom.Visible = false;
            // 
            // question_label4_createAssinmentFom
            // 
            this.question_label4_createAssinmentFom.AutoSize = true;
            this.question_label4_createAssinmentFom.Location = new System.Drawing.Point(4, 246);
            this.question_label4_createAssinmentFom.Name = "question_label4_createAssinmentFom";
            this.question_label4_createAssinmentFom.Size = new System.Drawing.Size(18, 19);
            this.question_label4_createAssinmentFom.TabIndex = 8;
            this.question_label4_createAssinmentFom.Text = "C";
            this.question_label4_createAssinmentFom.Visible = false;
            // 
            // question_label3_createAssinmentFom
            // 
            this.question_label3_createAssinmentFom.AutoSize = true;
            this.question_label3_createAssinmentFom.Location = new System.Drawing.Point(4, 185);
            this.question_label3_createAssinmentFom.Name = "question_label3_createAssinmentFom";
            this.question_label3_createAssinmentFom.Size = new System.Drawing.Size(17, 19);
            this.question_label3_createAssinmentFom.TabIndex = 7;
            this.question_label3_createAssinmentFom.Text = "B";
            this.question_label3_createAssinmentFom.Visible = false;
            // 
            // question_label2_createAssinmentFom
            // 
            this.question_label2_createAssinmentFom.AutoSize = true;
            this.question_label2_createAssinmentFom.Location = new System.Drawing.Point(3, 124);
            this.question_label2_createAssinmentFom.Name = "question_label2_createAssinmentFom";
            this.question_label2_createAssinmentFom.Size = new System.Drawing.Size(18, 19);
            this.question_label2_createAssinmentFom.TabIndex = 6;
            this.question_label2_createAssinmentFom.Text = "A";
            this.question_label2_createAssinmentFom.Visible = false;
            // 
            // question_label1_createAssinmentFom
            // 
            this.question_label1_createAssinmentFom.AutoSize = true;
            this.question_label1_createAssinmentFom.Location = new System.Drawing.Point(4, 63);
            this.question_label1_createAssinmentFom.Name = "question_label1_createAssinmentFom";
            this.question_label1_createAssinmentFom.Size = new System.Drawing.Size(61, 19);
            this.question_label1_createAssinmentFom.TabIndex = 5;
            this.question_label1_createAssinmentFom.Text = "Question";
            this.question_label1_createAssinmentFom.Visible = false;
            // 
            // formCreateAssignment_btnCreateAssignment
            // 
            this.formCreateAssignment_btnCreateAssignment.Location = new System.Drawing.Point(287, 9);
            this.formCreateAssignment_btnCreateAssignment.Name = "formCreateAssignment_btnCreateAssignment";
            this.formCreateAssignment_btnCreateAssignment.Size = new System.Drawing.Size(111, 33);
            this.formCreateAssignment_btnCreateAssignment.TabIndex = 4;
            this.formCreateAssignment_btnCreateAssignment.Text = "Create Assignment";
            this.formCreateAssignment_btnCreateAssignment.Click += new System.EventHandler(this.formCreateAssignment_btnCreateAssignment_Click);
            // 
            // formCreateAssignment_textboxNumQuestions
            // 
            this.formCreateAssignment_textboxNumQuestions.Location = new System.Drawing.Point(145, 14);
            this.formCreateAssignment_textboxNumQuestions.Name = "formCreateAssignment_textboxNumQuestions";
            this.formCreateAssignment_textboxNumQuestions.Size = new System.Drawing.Size(115, 23);
            this.formCreateAssignment_textboxNumQuestions.TabIndex = 3;
            // 
            // numberofquestions_label
            // 
            this.numberofquestions_label.AutoSize = true;
            this.numberofquestions_label.Location = new System.Drawing.Point(4, 14);
            this.numberofquestions_label.Name = "numberofquestions_label";
            this.numberofquestions_label.Size = new System.Drawing.Size(135, 19);
            this.numberofquestions_label.TabIndex = 2;
            this.numberofquestions_label.Text = "Number of Questions";
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(561, 14);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(120, 95);
            this.listBox1.TabIndex = 19;
            // 
            // formCreateAssignment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(900, 600);
            this.Controls.Add(this.metroTabControl1);
            this.MaximizeBox = false;
            this.Name = "formCreateAssignment";
            this.Style = MetroFramework.MetroColorStyle.Black;
            this.Text = "Create Assignment";
            this.Load += new System.EventHandler(this.formCreateAssignmentNew_Load);
            this.metroTabControl1.ResumeLayout(false);
            this.formCreateAssignment_create.ResumeLayout(false);
            this.formCreateAssignment_create.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroTabControl metroTabControl1;
        private MetroFramework.Controls.MetroTabPage formCreateAssignment_create;
        private MetroFramework.Controls.MetroTextBox formCreateAssignment_textboxNumQuestions;
        private MetroFramework.Controls.MetroLabel numberofquestions_label;
        private MetroFramework.Controls.MetroButton formCreateAssignment_btnCreateAssignment;
        private MetroFramework.Controls.MetroLabel question_label6_createAssinmentFom;
        private MetroFramework.Controls.MetroLabel question_label5_createAssinmentFom;
        private MetroFramework.Controls.MetroLabel question_label4_createAssinmentFom;
        private MetroFramework.Controls.MetroLabel question_label3_createAssinmentFom;
        private MetroFramework.Controls.MetroLabel question_label2_createAssinmentFom;
        private MetroFramework.Controls.MetroLabel question_label1_createAssinmentFom;
        private MetroFramework.Controls.MetroTextBox formCreateAssignment_textboxCorrectAnswer;
        private MetroFramework.Controls.MetroLabel question_label7_createAssinmentFom;
        private MetroFramework.Controls.MetroButton createAssignment_next_button;
        private MetroFramework.Controls.MetroTextBox formCreateAssignment_textboxAnswerD;
        private MetroFramework.Controls.MetroTextBox formCreateAssignment_textboxAnswerC;
        private MetroFramework.Controls.MetroTextBox formCreateAssignment_textboxAnswerB;
        private MetroFramework.Controls.MetroTextBox formCreateAssignment_textboxAnswerA;
        private MetroFramework.Controls.MetroTextBox formCreateAssignment_textboxQuestion;
        private System.Windows.Forms.ListBox listBox1;
    }
}