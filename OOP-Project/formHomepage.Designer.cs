﻿namespace OOP_Project
{
    partial class formHomepage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroTabControl1 = new MetroFramework.Controls.MetroTabControl();
            this.formHomepage_tabMyCourses = new MetroFramework.Controls.MetroTabPage();
            this.create_course_button = new MetroFramework.Controls.MetroButton();
            this.courses_listView = new System.Windows.Forms.ListView();
            this.formHomepage_tabSearchCourses = new MetroFramework.Controls.MetroTabPage();
            this.formHomepage_name_label_Description = new MetroFramework.Controls.MetroLabel();
            this.formHomepage_name_label_ID = new MetroFramework.Controls.MetroLabel();
            this.formHomepage_name_label_name = new MetroFramework.Controls.MetroLabel();
            this.search_instructor_id = new MetroFramework.Controls.MetroLabel();
            this.search_name = new MetroFramework.Controls.MetroLabel();
            this.search_description = new MetroFramework.Controls.MetroLabel();
            this.formHomepage_search_btnEnroll = new MetroFramework.Controls.MetroButton();
            this.formHomepage_search_btnSearch = new MetroFramework.Controls.MetroButton();
            this.formHomepage_search_textboxCourseID = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.formHomepage_btnLogout = new MetroFramework.Controls.MetroButton();
            this.metroTabControl1.SuspendLayout();
            this.formHomepage_tabMyCourses.SuspendLayout();
            this.formHomepage_tabSearchCourses.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroTabControl1
            // 
            this.metroTabControl1.Controls.Add(this.formHomepage_tabMyCourses);
            this.metroTabControl1.Controls.Add(this.formHomepage_tabSearchCourses);
            this.metroTabControl1.FontWeight = MetroFramework.MetroTabControlWeight.Bold;
            this.metroTabControl1.Location = new System.Drawing.Point(23, 63);
            this.metroTabControl1.Name = "metroTabControl1";
            this.metroTabControl1.SelectedIndex = 0;
            this.metroTabControl1.Size = new System.Drawing.Size(854, 514);
            this.metroTabControl1.Style = MetroFramework.MetroColorStyle.Black;
            this.metroTabControl1.TabIndex = 0;
            this.metroTabControl1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.metroTabControl1.Theme = MetroFramework.MetroThemeStyle.Light;
            // 
            // formHomepage_tabMyCourses
            // 
            this.formHomepage_tabMyCourses.Controls.Add(this.create_course_button);
            this.formHomepage_tabMyCourses.Controls.Add(this.courses_listView);
            this.formHomepage_tabMyCourses.HorizontalScrollbarBarColor = true;
            this.formHomepage_tabMyCourses.Location = new System.Drawing.Point(4, 35);
            this.formHomepage_tabMyCourses.Name = "formHomepage_tabMyCourses";
            this.formHomepage_tabMyCourses.Size = new System.Drawing.Size(846, 475);
            this.formHomepage_tabMyCourses.TabIndex = 0;
            this.formHomepage_tabMyCourses.Text = "                        My Courses                        ";
            this.formHomepage_tabMyCourses.VerticalScrollbarBarColor = true;
            this.formHomepage_tabMyCourses.Click += new System.EventHandler(this.formHomepage_tabMyCourses_Click);
            // 
            // create_course_button
            // 
            this.create_course_button.Location = new System.Drawing.Point(723, 388);
            this.create_course_button.Name = "create_course_button";
            this.create_course_button.Size = new System.Drawing.Size(111, 34);
            this.create_course_button.TabIndex = 5;
            this.create_course_button.Text = "Add Course";
            this.create_course_button.Click += new System.EventHandler(this.create_course_button_Click);
            // 
            // courses_listView
            // 
            this.courses_listView.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.courses_listView.Location = new System.Drawing.Point(12, 13);
            this.courses_listView.Name = "courses_listView";
            //this.courses_listView.RoundedCornersMaskListItem = ((byte)(15));
            this.courses_listView.Size = new System.Drawing.Size(822, 346);
            this.courses_listView.TabIndex = 3;
            //this.courses_listView.VIBlendScrollBarsTheme = VIBlend.Utilities.VIBLEND_THEME.EXPRESSIONDARK;
            //this.courses_listView.VIBlendTheme = VIBlend.Utilities.VIBLEND_THEME.EXPRESSIONDARK;
            //this.courses_listView.ItemMouseDown += new System.EventHandler<VIBlend.WinForms.Controls.ListItemMouseEventArgs>(this.courses_listView_ItemMouseDown);
            this.courses_listView.Click += new System.EventHandler(this.vListBox1_Click);
            // 
            // formHomepage_tabSearchCourses
            // 
            this.formHomepage_tabSearchCourses.Controls.Add(this.formHomepage_name_label_Description);
            this.formHomepage_tabSearchCourses.Controls.Add(this.formHomepage_name_label_ID);
            this.formHomepage_tabSearchCourses.Controls.Add(this.formHomepage_name_label_name);
            this.formHomepage_tabSearchCourses.Controls.Add(this.search_instructor_id);
            this.formHomepage_tabSearchCourses.Controls.Add(this.search_name);
            this.formHomepage_tabSearchCourses.Controls.Add(this.search_description);
            this.formHomepage_tabSearchCourses.Controls.Add(this.formHomepage_search_btnEnroll);
            this.formHomepage_tabSearchCourses.Controls.Add(this.formHomepage_search_btnSearch);
            this.formHomepage_tabSearchCourses.Controls.Add(this.formHomepage_search_textboxCourseID);
            this.formHomepage_tabSearchCourses.Controls.Add(this.metroLabel1);
            this.formHomepage_tabSearchCourses.HorizontalScrollbarBarColor = true;
            this.formHomepage_tabSearchCourses.Location = new System.Drawing.Point(4, 35);
            this.formHomepage_tabSearchCourses.Name = "formHomepage_tabSearchCourses";
            this.formHomepage_tabSearchCourses.Size = new System.Drawing.Size(846, 475);
            this.formHomepage_tabSearchCourses.TabIndex = 1;
            this.formHomepage_tabSearchCourses.Text = "                        Search Courses                        ";
            this.formHomepage_tabSearchCourses.VerticalScrollbarBarColor = true;
            this.formHomepage_tabSearchCourses.Click += new System.EventHandler(this.formHomepage_tabSearchCourses_Click);
            // 
            // formHomepage_name_label_Description
            // 
            this.formHomepage_name_label_Description.AutoSize = true;
            this.formHomepage_name_label_Description.Location = new System.Drawing.Point(322, 101);
            this.formHomepage_name_label_Description.Name = "formHomepage_name_label_Description";
            this.formHomepage_name_label_Description.Size = new System.Drawing.Size(74, 19);
            this.formHomepage_name_label_Description.TabIndex = 11;
            this.formHomepage_name_label_Description.Text = "Description";
            this.formHomepage_name_label_Description.Click += new System.EventHandler(this.metroLabel4_Click);
            // 
            // formHomepage_name_label_ID
            // 
            this.formHomepage_name_label_ID.AutoSize = true;
            this.formHomepage_name_label_ID.Location = new System.Drawing.Point(322, 67);
            this.formHomepage_name_label_ID.Name = "formHomepage_name_label_ID";
            this.formHomepage_name_label_ID.Size = new System.Drawing.Size(79, 19);
            this.formHomepage_name_label_ID.TabIndex = 10;
            this.formHomepage_name_label_ID.Text = "Instructor ID";
            // 
            // formHomepage_name_label_name
            // 
            this.formHomepage_name_label_name.AutoSize = true;
            this.formHomepage_name_label_name.Location = new System.Drawing.Point(322, 32);
            this.formHomepage_name_label_name.Name = "formHomepage_name_label_name";
            this.formHomepage_name_label_name.Size = new System.Drawing.Size(45, 19);
            this.formHomepage_name_label_name.TabIndex = 9;
            this.formHomepage_name_label_name.Text = "Name";
            // 
            // search_instructor_id
            // 
            this.search_instructor_id.AutoSize = true;
            this.search_instructor_id.Location = new System.Drawing.Point(486, 67);
            this.search_instructor_id.Name = "search_instructor_id";
            this.search_instructor_id.Size = new System.Drawing.Size(0, 0);
            this.search_instructor_id.TabIndex = 8;
            // 
            // search_name
            // 
            this.search_name.AutoSize = true;
            this.search_name.Location = new System.Drawing.Point(486, 32);
            this.search_name.Name = "search_name";
            this.search_name.Size = new System.Drawing.Size(0, 0);
            this.search_name.TabIndex = 7;
            // 
            // search_description
            // 
            this.search_description.AutoSize = true;
            this.search_description.Location = new System.Drawing.Point(486, 101);
            this.search_description.Name = "search_description";
            this.search_description.Size = new System.Drawing.Size(0, 0);
            this.search_description.TabIndex = 6;
            // 
            // formHomepage_search_btnEnroll
            // 
            this.formHomepage_search_btnEnroll.Location = new System.Drawing.Point(758, 422);
            this.formHomepage_search_btnEnroll.Name = "formHomepage_search_btnEnroll";
            this.formHomepage_search_btnEnroll.Size = new System.Drawing.Size(75, 23);
            this.formHomepage_search_btnEnroll.TabIndex = 5;
            this.formHomepage_search_btnEnroll.Text = "Enroll";
            this.formHomepage_search_btnEnroll.Visible = false;
            this.formHomepage_search_btnEnroll.Click += new System.EventHandler(this.formHomepage_search_btnEnroll_Click);
            // 
            // formHomepage_search_btnSearch
            // 
            this.formHomepage_search_btnSearch.Location = new System.Drawing.Point(135, 67);
            this.formHomepage_search_btnSearch.Name = "formHomepage_search_btnSearch";
            this.formHomepage_search_btnSearch.Size = new System.Drawing.Size(111, 34);
            this.formHomepage_search_btnSearch.TabIndex = 4;
            this.formHomepage_search_btnSearch.Text = "Search";
            this.formHomepage_search_btnSearch.Click += new System.EventHandler(this.formHomepage_search_btnSearch_Click);
            // 
            // formHomepage_search_textboxCourseID
            // 
            this.formHomepage_search_textboxCourseID.Location = new System.Drawing.Point(114, 27);
            this.formHomepage_search_textboxCourseID.Name = "formHomepage_search_textboxCourseID";
            this.formHomepage_search_textboxCourseID.Size = new System.Drawing.Size(132, 23);
            this.formHomepage_search_textboxCourseID.TabIndex = 3;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(20, 32);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(66, 19);
            this.metroLabel1.TabIndex = 2;
            this.metroLabel1.Text = "Course ID";
            // 
            // formHomepage_btnLogout
            // 
            this.formHomepage_btnLogout.Location = new System.Drawing.Point(777, 33);
            this.formHomepage_btnLogout.Name = "formHomepage_btnLogout";
            this.formHomepage_btnLogout.Size = new System.Drawing.Size(100, 25);
            this.formHomepage_btnLogout.TabIndex = 1;
            this.formHomepage_btnLogout.Text = "Logout";
            this.formHomepage_btnLogout.Click += new System.EventHandler(this.formHomepage_btnLogout_Click);
            // 
            // formHomepage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(900, 600);
            this.Controls.Add(this.formHomepage_btnLogout);
            this.Controls.Add(this.metroTabControl1);
            this.MaximizeBox = false;
            this.Name = "formHomepage";
            this.Resizable = false;
            this.Style = MetroFramework.MetroColorStyle.Black;
            this.Text = "Homepage";
            this.Theme = MetroFramework.MetroThemeStyle.Light;
            this.Load += new System.EventHandler(this.formHomepage_Load);
            this.metroTabControl1.ResumeLayout(false);
            this.formHomepage_tabMyCourses.ResumeLayout(false);
            this.formHomepage_tabSearchCourses.ResumeLayout(false);
            this.formHomepage_tabSearchCourses.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroTabControl metroTabControl1;
        private MetroFramework.Controls.MetroTabPage formHomepage_tabMyCourses;
        private MetroFramework.Controls.MetroTabPage formHomepage_tabSearchCourses;
        private MetroFramework.Controls.MetroButton formHomepage_search_btnEnroll;
        private MetroFramework.Controls.MetroButton formHomepage_search_btnSearch;
        private MetroFramework.Controls.MetroTextBox formHomepage_search_textboxCourseID;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel search_instructor_id;
        private MetroFramework.Controls.MetroLabel search_description;
        private MetroFramework.Controls.MetroLabel search_name;
        private MetroFramework.Controls.MetroLabel formHomepage_name_label_Description;
        private MetroFramework.Controls.MetroLabel formHomepage_name_label_ID;
        private MetroFramework.Controls.MetroLabel formHomepage_name_label_name;
        private System.Windows.Forms.ListView courses_listView;
        private MetroFramework.Controls.MetroButton create_course_button;
        private MetroFramework.Controls.MetroButton formHomepage_btnLogout;
    }
}