﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Project
{
    class clsQuestion
    {
        public static string ID;
        string quest;
        string[] answers;
        public string  correctAnswer, selectedAnswer;
        public clsQuestion(){
            answers = new string[4];
        }
        public bool isCorrect(){
            if (correctAnswer == selectedAnswer) return true;
            else return false;
        }
        public void setQuestion(string q){
            quest = q;
        }
        public void setAnswers(string a, string b, string c, string d){
            answers[0] = a;
            answers[1] = b;
            answers[2] = c;
            answers[3] = d;
        }
        public void setCorrect(string correct){
            correctAnswer = correct;
        }
        public bool checkAnswer(){
            if (correctAnswer == selectedAnswer) return true; else return false;
        }

        public string GetQuestion()
        {
            return quest;
        }
        public string[] GetAnswers()
        {
            return answers;
        }
    }
}
