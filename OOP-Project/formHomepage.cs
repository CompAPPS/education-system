﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using System.Data.SqlClient;


namespace OOP_Project
{
    public partial class formHomepage : MetroForm
    {
        SqlConnection con = new SqlConnection(@"Data Source=AHMED\SQLEXPRESS;Initial Catalog=oop_project;Integrated Security=True;MultipleActiveResultSets=True;");
        SqlCommand cmd = new SqlCommand();
        SqlDataReader dr;
        static string courseName;
        string user_ID = clsStudent.username;
        List<string> courseIDs = new List<string>();
        public formHomepage()
        {
            try
            {
                InitializeComponent();
                cmd.Connection = con;
                //if student
                con.Open();
                if (clsPerson.acc_type == "student")
                {
                    formHomepage_search_btnEnroll.Visible = true;
                    string Query = "SELECT * FROM StudentCourse WHERE personID ='" + user_ID + "'";
                    cmd = new SqlCommand(Query, con);
                    dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        courseIDs.Add(dr.GetInt64(1).ToString());
                    }

                    for (int i = 0; i < courseIDs.Count; i++)
                    {
                        string Query1 = "SELECT * FROM Course WHERE courseID ='" + courseIDs[i] + "'";
                        cmd = new SqlCommand(Query1, con);
                        dr = cmd.ExecuteReader();
                        int counter = 0;
                        while (dr.Read())
                        {
                            ListViewItem item = new ListViewItem();
                            item.Text = dr.GetString(0);
                            courses_listView.Items.Add(item.Text);
                            counter++;
                        }
                    }
                }
                else//instructor
                {
                    List<string> instructor_course = new List<string>();
                    string Query = "SELECT * FROM Course WHERE instructorID ='" + clsInstructor.username + "'";
                    cmd = new SqlCommand(Query, con);
                    dr = cmd.ExecuteReader();
                    int counter = 0;
                    while (dr.Read())
                    {
                        courseIDs.Add(dr.GetInt64(3).ToString());
                        instructor_course.Add(dr.GetString(0));
                    }

                    for (int i = 0; i < instructor_course.Count; i++)
                    {
                        ListViewItem item = new ListViewItem();
                        item.Text = instructor_course[i];
                        courses_listView.Items.Add(item.Text);
                        counter++;
                    }
                }
                con.Close();
                if (clsStudent.acc_type == "student")
                {
                    create_course_button.Hide();
                }
            }
            catch { MetroMessageBox x = new MetroMessageBox(); x.Text = "Can't connect to database..."; x.Show(); }
        }

        private void formHomepage_Load(object sender, EventArgs e)
        {

        }

        private void formHomepage_search_btnSearch_Click(object sender, EventArgs e)
        {
            con.Open();
            string query = "SELECT * FROM Course WHERE courseID ='" + formHomepage_search_textboxCourseID.Text + "'";
            SqlCommand cmd = new SqlCommand(query, con);
            dr = cmd.ExecuteReader();
            bool n = false;
            while (dr.Read())
            {
             search_description.Text=dr.GetString(1);
             search_name.Text = dr.GetString(0);
             search_instructor_id.Text = dr.GetString(2);
                n=true;
            }
                if (!n)
                {
                    MetroMessageBox box = new MetroMessageBox();
                    box.Text = "Invalid ID";
                    box.Show();
                }
            con.Close();
        }

        private void metroLabel3_Click(object sender, EventArgs e)
        {

        }

        private void formHomepage_tabSearchCourses_Click(object sender, EventArgs e)
        {

        }

        private void metroLabel4_Click(object sender, EventArgs e)
        {

        }

        private void formHomepage_search_btnEnroll_Click(object sender, EventArgs e)
        {
            con.Open();
            cmd.CommandText = "insert into StudentCourse values('" +clsStudent.username +"','"+formHomepage_search_textboxCourseID.Text+"')";
            try
            {
                cmd.ExecuteNonQuery();
                con.Close();
                MessageBox.Show("Done");
            }
            catch (System.Data.SqlClient.SqlException)
            {
                MessageBox.Show( "You are arleady enrolled in this course");
            }
           
            this.Close();
            formHomepage x = new formHomepage();
            x.Show();
        }

        private void formHomepage_tabMyCourses_Click(object sender, EventArgs e)
        {

        }

        private void courses_listView_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void vListBox1_Click(object sender, EventArgs e)
        {
        }

        private void formHomepage_tabMyCourses_btnGoToCourse_Click(object sender, EventArgs e)
        {
            this.Close();
            formCourse form = new formCourse();
            form.Show();
        }

        private void create_course_button_Click(object sender, EventArgs e)
        {
            this.Close();
            formCreateCourse create = new formCreateCourse();
            create.Show();
        }

        private void courses_listView_ItemMouseDown(object sender, EventArgs e)
        {           
            clsCourse.ID = courseIDs[courses_listView.SelectedIndices[0]];
            courseName = courses_listView.SelectedItems[0].Text;             
            this.Close();
            formCourse create = new formCourse();
            create.Show();
        }

        private void formHomepage_btnLogout_Click(object sender, EventArgs e)
        {
            clsPerson.acc_type = "";
            clsPerson.username = "";
            clsCourse.ID = "";
            clasAssignment.AssignmentID = "";
            clasAssignment.numberofQuestions = 0;
            this.Close();
            formLoginRegister x = new formLoginRegister();
            x.Show();
        }
    }
}
