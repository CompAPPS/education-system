﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Project
{
    class clasAssignment
    {
        public long courseID;
        clsQuestion[] questions;
        public static string AssignmentID;
        public static long numberofQuestions;
        int counter;
        public clasAssignment(){
            questions = new clsQuestion[numberofQuestions];
            counter = 0;
        }
        public void setNumQuestions(long num){
            numberofQuestions = num;
        }
        public void setQuestion(string QQ, string ans1, string ans2, string ans3, string ans4, string  corr){
            questions[counter].setQuestion(QQ);
            questions[counter].setAnswers(ans1, ans2, ans3, ans4);
            questions[counter].setCorrect(corr);
            counter++;
        }
        
    }
}
