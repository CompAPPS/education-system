﻿namespace OOP_Project
{
    partial class FormAssignment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {            this.metroTabControl1 = new MetroFramework.Controls.MetroTabControl();
            this.formAssignment_questions = new MetroFramework.Controls.MetroTabPage();
            this.Score_Lbl = new MetroFramework.Controls.MetroLabel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Submit_Btn = new MetroFramework.Controls.MetroButton();
            this.Answer4_Rbtn = new MetroFramework.Controls.MetroRadioButton();
            this.Answer3_Rbtn = new MetroFramework.Controls.MetroRadioButton();
            this.Answer2_Rbtn = new MetroFramework.Controls.MetroRadioButton();
            this.Answer1_Rbtn = new MetroFramework.Controls.MetroRadioButton();
            this.Ques_Label = new MetroFramework.Controls.MetroLabel();
            this.Ques_ListView = new System.Windows.Forms.ListView();
            this.formAssignment_btnBack = new MetroFramework.Controls.MetroButton();
            this.metroTabControl1.SuspendLayout();
            this.formAssignment_questions.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroTabControl1
            // 
            this.metroTabControl1.Controls.Add(this.formAssignment_questions);
            this.metroTabControl1.FontWeight = MetroFramework.MetroTabControlWeight.Bold;
            this.metroTabControl1.Location = new System.Drawing.Point(24, 64);
            this.metroTabControl1.Name = "metroTabControl1";
            this.metroTabControl1.SelectedIndex = 0;
            this.metroTabControl1.Size = new System.Drawing.Size(853, 502);
            this.metroTabControl1.Style = MetroFramework.MetroColorStyle.Black;
            this.metroTabControl1.TabIndex = 0;
            // 
            // formAssignment_questions
            // 
            this.formAssignment_questions.Controls.Add(this.Score_Lbl);
            this.formAssignment_questions.Controls.Add(this.groupBox1);
            this.formAssignment_questions.Controls.Add(this.Ques_ListView);
            this.formAssignment_questions.HorizontalScrollbarBarColor = true;
            this.formAssignment_questions.Location = new System.Drawing.Point(4, 35);
            this.formAssignment_questions.Name = "formAssignment_questions";
            this.formAssignment_questions.Size = new System.Drawing.Size(845, 463);
            this.formAssignment_questions.TabIndex = 0;
            this.formAssignment_questions.Text = "                       Questions                        ";
            this.formAssignment_questions.VerticalScrollbarBarColor = true;
            // 
            // Score_Lbl
            // 
            this.Score_Lbl.AutoSize = true;
            this.Score_Lbl.Location = new System.Drawing.Point(713, 4);
            this.Score_Lbl.Name = "Score_Lbl";
            this.Score_Lbl.Size = new System.Drawing.Size(53, 19);
            this.Score_Lbl.TabIndex = 4;
            this.Score_Lbl.Text = "Score : ";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox1.Controls.Add(this.Submit_Btn);
            this.groupBox1.Controls.Add(this.Answer4_Rbtn);
            this.groupBox1.Controls.Add(this.Answer3_Rbtn);
            this.groupBox1.Controls.Add(this.Answer2_Rbtn);
            this.groupBox1.Controls.Add(this.Answer1_Rbtn);
            this.groupBox1.Controls.Add(this.Ques_Label);
            this.groupBox1.Location = new System.Drawing.Point(121, 26);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(690, 422);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            // 
            // Submit_Btn
            // 
            this.Submit_Btn.Location = new System.Drawing.Point(573, 383);
            this.Submit_Btn.Name = "Submit_Btn";
            this.Submit_Btn.Size = new System.Drawing.Size(111, 33);
            this.Submit_Btn.TabIndex = 5;
            this.Submit_Btn.Text = "Submit";
            this.Submit_Btn.Click += new System.EventHandler(this.Submit_Btn_Click);
            // 
            // Answer4_Rbtn
            // 
            this.Answer4_Rbtn.AutoSize = true;
            this.Answer4_Rbtn.Location = new System.Drawing.Point(43, 275);
            this.Answer4_Rbtn.Name = "Answer4_Rbtn";
            this.Answer4_Rbtn.Size = new System.Drawing.Size(31, 15);
            this.Answer4_Rbtn.TabIndex = 4;
            this.Answer4_Rbtn.Text = "D";
            this.Answer4_Rbtn.UseVisualStyleBackColor = true;
            this.Answer4_Rbtn.Visible = false;
            // 
            // Answer3_Rbtn
            // 
            this.Answer3_Rbtn.AutoSize = true;
            this.Answer3_Rbtn.Location = new System.Drawing.Point(43, 207);
            this.Answer3_Rbtn.Name = "Answer3_Rbtn";
            this.Answer3_Rbtn.Size = new System.Drawing.Size(31, 15);
            this.Answer3_Rbtn.TabIndex = 3;
            this.Answer3_Rbtn.Text = "C";
            this.Answer3_Rbtn.UseVisualStyleBackColor = true;
            this.Answer3_Rbtn.Visible = false;
            // 
            // Answer2_Rbtn
            // 
            this.Answer2_Rbtn.AutoSize = true;
            this.Answer2_Rbtn.Location = new System.Drawing.Point(43, 141);
            this.Answer2_Rbtn.Name = "Answer2_Rbtn";
            this.Answer2_Rbtn.Size = new System.Drawing.Size(30, 15);
            this.Answer2_Rbtn.TabIndex = 2;
            this.Answer2_Rbtn.Text = "B";
            this.Answer2_Rbtn.UseVisualStyleBackColor = true;
            this.Answer2_Rbtn.Visible = false;
            // 
            // Answer1_Rbtn
            // 
            this.Answer1_Rbtn.AutoSize = true;
            this.Answer1_Rbtn.Checked = true;
            this.Answer1_Rbtn.Location = new System.Drawing.Point(43, 71);
            this.Answer1_Rbtn.Name = "Answer1_Rbtn";
            this.Answer1_Rbtn.Size = new System.Drawing.Size(31, 15);
            this.Answer1_Rbtn.TabIndex = 1;
            this.Answer1_Rbtn.TabStop = true;
            this.Answer1_Rbtn.Text = "A";
            this.Answer1_Rbtn.UseVisualStyleBackColor = true;
            this.Answer1_Rbtn.Visible = false;
            // 
            // Ques_Label
            // 
            this.Ques_Label.AutoSize = true;
            this.Ques_Label.Location = new System.Drawing.Point(43, 28);
            this.Ques_Label.Name = "Ques_Label";
            this.Ques_Label.Size = new System.Drawing.Size(61, 19);
            this.Ques_Label.TabIndex = 0;
            this.Ques_Label.Text = "Question";
            this.Ques_Label.Visible = false;
            // 
            // Ques_ListView
            // 
            this.Ques_ListView.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F);
            this.Ques_ListView.Location = new System.Drawing.Point(3, 26);
            this.Ques_ListView.Name = "Ques_ListView";
            this.Ques_ListView.Size = new System.Drawing.Size(89, 422);
            this.Ques_ListView.TabIndex = 2;
            this.Ques_ListView.Text = "vListBox1";
            this.Ques_ListView.UseCompatibleStateImageBehavior = false;
            // 
            // formAssignment_btnBack
            // 
            this.formAssignment_btnBack.Location = new System.Drawing.Point(777, 33);
            this.formAssignment_btnBack.Name = "formAssignment_btnBack";
            this.formAssignment_btnBack.Size = new System.Drawing.Size(100, 25);
            this.formAssignment_btnBack.TabIndex = 1;
            this.formAssignment_btnBack.Text = "Back";
            this.formAssignment_btnBack.Click += new System.EventHandler(this.formAssignment_btnBack_Click);
            // 
            // FormAssignment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(900, 600);
            this.Controls.Add(this.formAssignment_btnBack);
            this.Controls.Add(this.metroTabControl1);
            this.Name = "FormAssignment";
            this.Style = MetroFramework.MetroColorStyle.Black;
            this.Text = "Assignment";
            this.Load += new System.EventHandler(this.ViewAssignment_Load);
            this.metroTabControl1.ResumeLayout(false);
            this.formAssignment_questions.ResumeLayout(false);
            this.formAssignment_questions.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroTabControl metroTabControl1;
        private MetroFramework.Controls.MetroTabPage formAssignment_questions;
        private System.Windows.Forms.GroupBox groupBox1;
        private MetroFramework.Controls.MetroButton Submit_Btn;
        private MetroFramework.Controls.MetroRadioButton Answer4_Rbtn;
        private MetroFramework.Controls.MetroRadioButton Answer3_Rbtn;
        private MetroFramework.Controls.MetroRadioButton Answer2_Rbtn;
        private MetroFramework.Controls.MetroRadioButton Answer1_Rbtn;
        private MetroFramework.Controls.MetroLabel Ques_Label;
        private MetroFramework.Controls.MetroLabel Score_Lbl;
        private MetroFramework.Controls.MetroButton formAssignment_btnBack;
        private System.Windows.Forms.ListView Ques_ListView;
    }
}