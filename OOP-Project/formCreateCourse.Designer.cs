﻿namespace OOP_Project
{
    partial class formCreateCourse
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.formCreateCourse_tabs = new MetroFramework.Controls.MetroTabControl();
            this.formCreateCourse_courseInfo = new MetroFramework.Controls.MetroTabPage();
            this.formCreateCourse_btnCreateAssignment = new MetroFramework.Controls.MetroButton();
            this.formCreateCourse_textboxDescription = new MetroFramework.Controls.MetroTextBox();
            this.formCreateCourse_textboxName = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.formCreateCourse_tabs.SuspendLayout();
            this.formCreateCourse_courseInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // formCreateCourse_tabs
            // 
            this.formCreateCourse_tabs.Controls.Add(this.formCreateCourse_courseInfo);
            this.formCreateCourse_tabs.FontWeight = MetroFramework.MetroTabControlWeight.Bold;
            this.formCreateCourse_tabs.Location = new System.Drawing.Point(24, 64);
            this.formCreateCourse_tabs.Name = "formCreateCourse_tabs";
            this.formCreateCourse_tabs.SelectedIndex = 0;
            this.formCreateCourse_tabs.Size = new System.Drawing.Size(853, 513);
            this.formCreateCourse_tabs.Style = MetroFramework.MetroColorStyle.Black;
            this.formCreateCourse_tabs.TabIndex = 0;
            // 
            // formCreateCourse_courseInfo
            // 
            this.formCreateCourse_courseInfo.Controls.Add(this.formCreateCourse_btnCreateAssignment);
            this.formCreateCourse_courseInfo.Controls.Add(this.formCreateCourse_textboxDescription);
            this.formCreateCourse_courseInfo.Controls.Add(this.formCreateCourse_textboxName);
            this.formCreateCourse_courseInfo.Controls.Add(this.metroLabel2);
            this.formCreateCourse_courseInfo.Controls.Add(this.metroLabel1);
            this.formCreateCourse_courseInfo.HorizontalScrollbarBarColor = true;
            this.formCreateCourse_courseInfo.Location = new System.Drawing.Point(4, 35);
            this.formCreateCourse_courseInfo.Name = "formCreateCourse_courseInfo";
            this.formCreateCourse_courseInfo.Size = new System.Drawing.Size(845, 474);
            this.formCreateCourse_courseInfo.TabIndex = 0;
            this.formCreateCourse_courseInfo.Text = "                        Course Info                        ";
            this.formCreateCourse_courseInfo.VerticalScrollbarBarColor = true;
            this.formCreateCourse_courseInfo.Click += new System.EventHandler(this.formCreateCourse_courseInfo_Click);
            // 
            // formCreateCourse_btnCreateAssignment
            // 
            this.formCreateCourse_btnCreateAssignment.Location = new System.Drawing.Point(622, 448);
            this.formCreateCourse_btnCreateAssignment.Name = "formCreateCourse_btnCreateAssignment";
            this.formCreateCourse_btnCreateAssignment.Size = new System.Drawing.Size(126, 25);
            this.formCreateCourse_btnCreateAssignment.TabIndex = 6;
            this.formCreateCourse_btnCreateAssignment.Text = "Create Course";
            this.formCreateCourse_btnCreateAssignment.Click += new System.EventHandler(this.formCreateCourse_btnCreateAssignment_Click);
            // 
            // formCreateCourse_textboxDescription
            // 
            this.formCreateCourse_textboxDescription.Location = new System.Drawing.Point(124, 83);
            this.formCreateCourse_textboxDescription.Name = "formCreateCourse_textboxDescription";
            this.formCreateCourse_textboxDescription.Size = new System.Drawing.Size(625, 350);
            this.formCreateCourse_textboxDescription.TabIndex = 5;
            // 
            // formCreateCourse_textboxName
            // 
            this.formCreateCourse_textboxName.Location = new System.Drawing.Point(124, 38);
            this.formCreateCourse_textboxName.Name = "formCreateCourse_textboxName";
            this.formCreateCourse_textboxName.Size = new System.Drawing.Size(288, 23);
            this.formCreateCourse_textboxName.TabIndex = 4;
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(4, 83);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(74, 19);
            this.metroLabel2.TabIndex = 3;
            this.metroLabel2.Text = "Description";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(4, 38);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(90, 19);
            this.metroLabel1.TabIndex = 2;
            this.metroLabel1.Text = "Course Name";
            // 
            // formCreateCourse
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(900, 600);
            this.Controls.Add(this.formCreateCourse_tabs);
            this.MaximizeBox = false;
            this.Name = "formCreateCourse";
            this.Style = MetroFramework.MetroColorStyle.Black;
            this.Text = "Create Course";
            this.Load += new System.EventHandler(this.Create_Course_Load);
            this.formCreateCourse_tabs.ResumeLayout(false);
            this.formCreateCourse_courseInfo.ResumeLayout(false);
            this.formCreateCourse_courseInfo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroTabControl formCreateCourse_tabs;
        private MetroFramework.Controls.MetroTabPage formCreateCourse_courseInfo;
        private MetroFramework.Controls.MetroTextBox formCreateCourse_textboxDescription;
        private MetroFramework.Controls.MetroTextBox formCreateCourse_textboxName;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroButton formCreateCourse_btnCreateAssignment;
    }
}