﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using System.Data.SqlClient;

namespace OOP_Project
{
    public partial class formCreateAssignment : MetroForm
    {
        SqlConnection con = new SqlConnection(@"Data Source=AHMED\SQLEXPRESS;Initial Catalog=oop_project;Integrated Security=True;MultipleActiveResultSets=True;");
        SqlCommand cmd = new SqlCommand();
        SqlDataReader dr;
        public formCreateAssignment()
        {
            cmd.Connection = con;
            InitializeComponent();
        }

        private void formCreateAssignmentNew_Load(object sender, EventArgs e)
        {

        }

        private void formCreateAssignment_btnCreateAssignment_Click(object sender, EventArgs e)
        {
            con.Open();
            cmd.CommandText = "insert into Assignment values('" + clsCourse.ID + "','" + formCreateAssignment_textboxNumQuestions.Text + "')";
            cmd.ExecuteNonQuery();
            string Query = "SELECT TOP 1 * FROM Assignment ORDER BY assignmentID DESC";
            cmd = new SqlCommand(Query, con);
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                clasAssignment.AssignmentID = dr.GetInt64(2).ToString();
            }
            clasAssignment.numberofQuestions = int.Parse(formCreateAssignment_textboxNumQuestions.Text);
            numberofquestions_label.Visible = false;
            formCreateAssignment_textboxNumQuestions.Visible = false;
            formCreateAssignment_btnCreateAssignment.Visible = false;
            question_label1_createAssinmentFom.Visible = true;
            question_label2_createAssinmentFom.Visible = true;
            question_label3_createAssinmentFom.Visible = true;
            question_label4_createAssinmentFom.Visible = true;
            question_label5_createAssinmentFom.Visible = true;
            question_label6_createAssinmentFom.Visible = true;
            question_label7_createAssinmentFom.Visible = true;
            formCreateAssignment_textboxQuestion.Visible = true;
            formCreateAssignment_textboxAnswerA.Visible = true;
            formCreateAssignment_textboxAnswerB.Visible = true;
            formCreateAssignment_textboxAnswerC.Visible = true;
            formCreateAssignment_textboxAnswerD.Visible = true;
            formCreateAssignment_textboxCorrectAnswer.Visible = true;
            question_label7_createAssinmentFom.Visible = true;
            createAssignment_next_button.Visible = true;
            con.Close();
        }

        private void createAssignment_next_button_Click(object sender, EventArgs e)
        {
            con.Open();
            cmd.CommandText = "insert into Question values('" + formCreateAssignment_textboxQuestion.Text +
               "','" + formCreateAssignment_textboxAnswerA.Text + "','" + formCreateAssignment_textboxAnswerB.Text
               + "','" + formCreateAssignment_textboxAnswerC.Text + "','" + formCreateAssignment_textboxAnswerD.Text
           + "','" + formCreateAssignment_textboxCorrectAnswer.Text + "','" + clasAssignment.AssignmentID + "')";
            cmd.ExecuteNonQuery();
            formCreateAssignment_textboxQuestion.Clear();
            formCreateAssignment_textboxAnswerA.Clear();
            formCreateAssignment_textboxAnswerB.Clear();
            formCreateAssignment_textboxAnswerC.Clear();
            formCreateAssignment_textboxAnswerD.Clear();
            formCreateAssignment_textboxCorrectAnswer.Clear();
            con.Close();
            clasAssignment.numberofQuestions--;
            if (clasAssignment.numberofQuestions == 1)
            {
                createAssignment_next_button.Text = "Finsh";
            }
                if (clasAssignment.numberofQuestions == 0)
            {
                createAssignment_next_button.Visible = false;
                this.Close();
                formHomepage x = new formHomepage();
                x.Show();
            }

        }

        private void formCreateAssignment_textboxQuestion_Click(object sender, EventArgs e)
        {

        }
    }
}
